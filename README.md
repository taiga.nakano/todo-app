# PHP Todo Application with Docker

このプロジェクトは、Dockerを利用したPHPベースのTodoアプリケーションです。

MySQLとPHP環境が組み込まれたコンテナを使用して、Todoアプリケーションをセットアップ可能です。

> 2023年10月より参加していた勉強会で作成していたコードを元に、TODOアプリケーションを作成しました。
> 勉強会が2024年1月時点で途中のため、機能として盛り込めるものだけ採用しています。

## 環境

### docker環境

下記のイメージを使用しています。

- php:8.2-apache
- mysql:8.0.28

### アプリケーションコード

/app配下のコードが今回作成したフレームワーク部分になります。

## 処理のイメージ

- URLアクセスは/var/www/html/public/index.php内で処理
- index.phpはオートローダー(クラスローダー)を実行
- URLに応じてRouterで処理を分岐
- コントローラーで処理を実行
- index.phpを返す

## 前提条件

このプロジェクトを実行するには、以下がインストールされている必要があります。

- Docker
- Docker Compose

## セットアップ手順

1. **リポジトリのクローン**:
    ```bash
    $ git clone git@gitlab.com:taiga.nakano/todo-app.git
    $ cd todo-app
    ```

2. **Dockerコンテナのビルドと起動**:
    ```
    $ docker-compose up -d
    ```

3. アプリケーションは `http://localhost` でアクセス可能です。

## ディレクトリ構造

```bash
./
├── README.md 本ファイル
├── app フレームワーク部分
│   ├── Core 処理のコア機能
│   │   ├── DB DB接続の処理
│   │   │   └── DBconnector.php
│   │   ├── Dispatcher.php オートローダーの実行
│   │   ├── Libs 設定ファイル、環境変数の読み込み処理
│   │   │   ├── Config.php 設定ファイル(DB接続情報等)の読み込み
│   │   │   └── Env.php 環境変数(サーバー設定値等)の読み込み
│   │   ├── Logger ログ出力処理
│   │   │   ├── Logger.php
│   │   │   └── MessageCodes.php エラー処理等のマッピング
│   │   └── Router
│   │       ├── Map
│   │       │   └── map.php URLとコントローラーのマッピング
│   │       └── Router.php URLのルーティング処理(コントローラー呼び出し)
│   ├── Utils 組み込み関数のラッピング(エラー処理)
│   │   ├── ConfigManager.php
│   │   ├── ControllerManager.php
│   │   ├── FileManager.php
│   │   └── RegexPatterns.php
│   ├── app
│   │   └── Controller コントローラー
│   │       ├── AbstractController.php
│   │       ├── ControllerInterface.php
│   │       └── TopController.php
│   ├── config
│   │   └── database
│   │       └── mysql.php mysql接続情報
│   ├── logs
│   │   └── app.log ログファイル
│   └── public
│       ├── index.css
│       └── index.php 初回実行ファイル
├── docker インフラ部分
│   ├── mysql
│   │   ├── Dockerfile
│   │   ├── init.d
│   │   │   └── make_todo.sql コンテナ立ち上げ時のテーブル作成ファイル
│   │   └── my.cnf
│   └── php
│       ├── Dockerfile
│       └── php.ini
└── docker-compose.yml
```
