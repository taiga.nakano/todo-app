<?php

namespace Core\DB;

use Core\Libs\Env;
use Core\Libs\Config;
use PDO;
use PDOException;
use RuntimeException;

class DBconnector
{
	private static ?PDO $connection = null;

	private string $_dbType;
	private array $_param = [];

	private function __construct()
	{
		$this->_dbType = Env::get("DB_CONNECTION");
		$this->_param = Config::get("database.".$this->_dbType);
		$dsn = sprintf("mysql:host=%s;dbname=%s;charset=%s", "todo-mysql", $this->_param['name'], $this->_param['encode']);
		try {
			self::$connection = new PDO($dsn, $this->_param['user'], $this->_param['password'], $this->_param['options']);
		} catch (PDOException $e) {
			echo "DB接続失敗：" . $e->getMessage();
		}
	}

	public static function getConnection():PDO
	{
		if (is_null(self::$connection)) {
			new self();
		}
		return self::$connection;
	}

	/**
	 * cloneの禁止
	 * @throws RuntimeException
	 */
	public function __clone()
	{
		throw new RuntimeException("cloneは禁止されています");
	}
}
