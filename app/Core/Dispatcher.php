<?php

namespace Core;

require_once(realpath(dirname(__DIR__)) . '/Utils/FileManager.php');

use Utils\FileManager;
use RuntimeException;
use Exception;

class Dispatcher
{
	private static ?object $_instance = null;

	private function __construct()
	{
		spl_autoload_register([self::class, '_autoLoader']);
	}

	/**
	 * インスタンスの取得
	 *
	 * @return self
	 */
	public static function getInstance(): self
	{
		if (is_null(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * クラスのオートローディングの実行
	 *
	 * @param string $class
	 * @return void
	 */
	private static function _autoLoader(string $class) :void
	{
		$rootDir = dirname(__DIR__);
		$filePath = str_replace('\\', '/', $class);
		$fullFilePath = sprintf('%s/%s.php', $rootDir, $filePath);
		try {
			FileManager::is_readable($fullFilePath);
		} catch (Exception $e) {
			throw $e;
		}
		require_once($fullFilePath);
	}

	/**
	 * cloneの禁止
	 * @throws RuntimeException
	 */
	public function __clone()
	{
		throw new RuntimeException("cloneは禁止されています");
	}
}
