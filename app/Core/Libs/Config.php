<?php

namespace Core\Libs;

use Exception;

class Config
{
	private const DOCUMENT_ROOT = "/var/www/html";
	private const CONFIG_DIR_NAME = "config";
	private const ERR_CONFIG_DIR_CHECK = "configディレクトリが存在しません";
	private const ERR_TARGET_NOT_FOUND = "対象のディレクトリおよびファイルが存在しません";
	private const ERR_KEY_NOT_FOUND = "検索対象のキーは対象の配列に存在しません";

	/**
	 * 引数を受け取りファイルから値もしくは配列を返す
	 *
	 * @param string $str
	 * @param string|null $subStr
	 * @return string|array
	 */
	public static function get(string $str, string $subStr = null): string|array
	{
		if (!self::_checkDirOrFileExists(self::CONFIG_DIR_NAME)) {
			throw new Exception(self::ERR_CONFIG_DIR_CHECK);
		}
		$targetFile = self::CONFIG_DIR_NAME;
		$argArr = explode(".", $str);
		$searchWordArr = explode(".", $str);
		foreach ($argArr as $arg) {
			$fileWithExtension = $targetFile . "/" . $arg . ".php";
			$directoryPath = $targetFile . "/" . $arg;

			// ファイルが存在する場合
			if (self::_checkDirOrFileExists($fileWithExtension)) {
				$targetFile = $fileWithExtension;
				array_shift($searchWordArr);
				break;
			}

			// ディレクトリが存在する場合
			if (self::_checkDirOrFileExists($directoryPath)) {
				$targetFile = $directoryPath;
				array_shift($searchWordArr);
				continue;
			}

			// ファイルもディレクトリも存在しない場合はエラー
			throw new Exception(self::ERR_TARGET_NOT_FOUND);

		}
		$targetFile = self::DOCUMENT_ROOT . "/" . $targetFile;
		$target = include $targetFile;
		$result = self::_searchValue($target, $searchWordArr, $subStr);
		return $result;
	}

	/**
	 * 対象のファイルから値を取得して返す
	 *
	 * @param array $target
	 * @param mixed $searchWordArr
	 * @return string|array
	 */
	private static function _searchValue(array $target, mixed $searchWordArr, ?string $subStr): string|array
	{
		if (!count($searchWordArr)) {
			return $target;
		}
		foreach ($searchWordArr as $key) {
			if (isset($target[$key])) {
				$target = $target[$key];
				continue;
			}
			if (!is_null($subStr)) {
				return $subStr;
			}
			throw new Exception(self::ERR_KEY_NOT_FOUND);
		}
		return $target;
	}

	/**
	 * 引数から対象のファイルおよびディレクトリの存在確認を行う
	 *
	 * @param [type] $dirOrFileName
	 * @return boolean
	 */
	private static function _checkDirOrFileExists(string $dirOrFileName): bool
	{
		if (file_exists(self::DOCUMENT_ROOT . "/" . $dirOrFileName)) {
			return true;
		}
		return false;
	}
}
