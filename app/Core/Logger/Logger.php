<?php

namespace Core\Logger;

use Core\Libs\Env;
use Core\Logger\MessageCodes;
use \RuntimeException;
use \DateTime;

final class Logger
{
	private static ?object $_instance = null;
	private static int $_logLevel;
	private static array $_messageCodes;
	private static array $_logLevelArr = [
		'INFO'		=>	0,
		'WARNING'	=>	1,
		'ERROR'		=>	2,
	];
	private static string $_logDir = '/var/www/html/logs/';
	private static string $_logName = 'app.log';

	private function __construct()
	{
		$logLevel = Env::get("LOG_LEVEL");
		self::$_logLevel = isset(self::$_logLevelArr[$logLevel]) ? self::$_logLevelArr[$logLevel] : 0;
		self::$_messageCodes = MessageCodes::$messageCodes;
	}

	/**
	 * インスタンスの取得
	 *
	 * @return self
	 */
	public static function getInstance(): self
	{
		if (is_null(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * ログの出力実行
	 *
	 * @param string $errorCode
	 * @return void
	 */
	public function putLog(string $errorCode): void
	{
		$dateTime = new DateTime();
		$timeStamp = $dateTime->format('Y-m-d H:i:s');
		$messageContents = self::$_messageCodes[$errorCode];
		$message = $messageContents['message'];
		$level = self::$_logLevelArr[$messageContents['level']];
		if (self::$_logLevel > $level) {
			return;
		}
		$logMessage = sprintf('[%s] (%s) [%s] %s', $messageContents['level'], $timeStamp, $errorCode, $message);
		$logFile = self::_getLogFile();
		file_put_contents($logFile, $logMessage . PHP_EOL, FILE_APPEND | LOCK_EX);
		return;
	}

	/**
	 * ログファイル名を返す
	 *
	 * @return string
	 */
	private static function _getLogFile(): string
	{
		return self::$_logDir . self::$_logName;
	}

	/**
	 * cloneの禁止
	 * @throws RuntimeException
	 */
	public function __clone()
	{
		throw new RuntimeException("cloneは禁止されています");
	}
}
