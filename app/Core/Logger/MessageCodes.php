<?php

namespace Core\Logger;

class MessageCodes
{
	public static array $messageCodes = [
		'PAGE_ACCESS' => [
			'message' => 'ページにアクセスされました',
			'level' => 'INFO'
		],
		'ADD_STATUS_UPDATED' => [
			'message' => 'TODOが追加されました',
			'level' => 'INFO'
		],
		'TODO_STATUS_UPDATED' => [
			'message' => 'TODOの状態が更新されました',
			'level' => 'INFO'
		],
		'TODO_DELETED' => [
			'message' => 'TODOが削除されました',
			'level' => 'INFO'
		],
	];
}
