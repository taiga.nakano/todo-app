<?php

namespace Core\Router\Map;

use Exception;

class Map
{
	/**
	 * URLとコントローラーのマッピングを定義
	 *
	 * @var array
	 */
	public static array $map = [
		''						=> 	'app\Controller\TopController:index',
		'/add'					=> 	'app\Controller\TopController:addTodo',
		'/toggle_status'		=> 	'app\Controller\TopController:toggleStatus',
		'/delete'				=> 	'app\Controller\TopController:deleteTodo',
	];
}
