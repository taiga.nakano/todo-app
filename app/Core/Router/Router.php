<?php

namespace Core\Router;

use Core\Router\Map\Map;
use Exception;

class Router
{
	const ERR_URL_NOT_FOUND = "404エラー";

	public static string $parseUrlArr;
	public static ?array $postArr;

	public function __construct(string $requestUri, ?array $post)
	{
		self::$parseUrlArr = self::parseUrl($requestUri);
		self::$postArr = $post;
	}

	/**
	 * URLを正規化する
	 *
	 * @param string $requestUri
	 * @return string
	 */
	private static function parseUrl(string $requestUri): string
	{
		$parseUrlArr = parse_url($requestUri);
		if (isset($parseUrlArr['query'])) {
			parse_str($parseUrlArr['query'], $parameters); // クエリ引数の受け取り
		}
		return rtrim(strtolower($parseUrlArr['path']), '/');
	}

	/**
	 * URLを受け取ってコントローラーの処理を実行する
	 *
	 * @param string $parseUrlArr
	 * @return mixed
	 */
	private static function dispatchRequest(string $parseUrlArr, ?array $post): mixed
	{
		if (array_key_exists($parseUrlArr, Map::$map)) {
			[$controllerName, $methodName]  = explode(':', Map::$map[$parseUrlArr]);
			$instance = new $controllerName($methodName);
			return $instance->$methodName($post);
		} else {
			throw new Exception(self::ERR_URL_NOT_FOUND);
		}
	}

	public function execute(): mixed
	{
		return self::dispatchRequest(self::$parseUrlArr, self::$postArr);
	}
}
