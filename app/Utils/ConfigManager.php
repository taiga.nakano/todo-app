<?php

namespace Utils;

use Exception;

/**
 * パラメータ設定処理の例外ハンドリングクラス
 */
class ConfigManager
{
	/**
	 * ini_setの例外ハンドリング
	 *
	 * @param string $option
	 * @param string|integer|float|boolean|null $value
	 * @return void
	 */
	public static function ini_set(string $option, string|int|float|bool|null $value): void
	{
		$result = ini_set($option, $value);
		if (!$result) {
			throw new Exception(sprintf("パラメータの設定に失敗しました(設定値 %s : %s)", $option, $value));
		}
	}
}
