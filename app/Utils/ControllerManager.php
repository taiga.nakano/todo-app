<?php

namespace Utils;

use Exception;

/**
 * コントローラー処理の例外ハンドリング
 */
class ControllerManager
{
	/**
	 * 対象のクラスにメソッドが存在しない場合の処理
	 *
	 * @param object|string $object_or_class
	 * @param string $method
	 * @return void
	 */
	public static function method_exists(object|string $object_or_class, string $method): void
	{
		if (!method_exists($object_or_class, $method)) {
			$className = is_object($object_or_class) ? $object_or_class::class : $object_or_class;
			throw new Exception(sprintf(
				"メソッドが存在しません(クラス名 %s : メソッド名 %s)",
				$className,
				$method
			));
		}
	}
}
