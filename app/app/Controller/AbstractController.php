<?php

namespace app\Controller;

use Exception;
use Utils\ControllerManager;

abstract class AbstractController
{
    public function __construct(string $methodName)
    {
        if (method_exists($this, $methodName)) {
        } else {
            throw new Exception("Method $methodName は " . get_class($this)."に存在しません");
        }
    }
}
