<?php

namespace app\Controller;

interface ControllerInterface
{
	public function index(): mixed;
}
