<?php

namespace app\Controller;

use app\Controller\ControllerInterface;
use app\Controller\AbstractController;
use Core\Logger\Logger;
use Core\DB\DBconnector;

class TopController extends AbstractController implements ControllerInterface
{
	/**
	 * トップページの表示(TODOデータのリスト表示)
	 *
	 * @return mixed
	 */
	public function index(): mixed
	{
		$sql = <<<'SQL'
			SELECT
				*
			FROM
				todos
			WHERE
				del_flg = 0
			ORDER BY
				status ASC,
				due_date ASC
		SQL;
		$pdo = DBconnector::getConnection();
		$result = $pdo->query($sql)->fetchAll();
		$logger = Logger::getInstance();
		$logger->putLog("PAGE_ACCESS");
		return $result;
	}

	/**
	 * 新規TODOの追加
	 *
	 * @param [type] $post
	 * @return void
	 */
	public static function addTodo($post): void
	{
		if (empty($post['what']) || empty($post['when'])) {
			header('Location: http://localhost/');
			exit();
		}
		$dueDate = $post['when'];
		$name = $post['what'];
		$sql = "INSERT INTO todos (due_date, name) VALUES (?, ?)";
		$pdo = DBconnector::getConnection();
		$stmt = $pdo->prepare($sql);
		$stmt->execute([$dueDate, $name]);
		$logger = Logger::getInstance();
		$logger->putLog("ADD_STATUS_UPDATED");
		header('Location: http://localhost/');
		exit();
	}

	/**
	 * ステータスの更新
	 *
	 * @param [type] $post
	 * @return void
	 */
	public static function toggleStatus($post): void
	{
		$targetId = $post['id'];
		$pdo = DBconnector::getConnection();
		$stmt = $pdo->prepare("SELECT status FROM todos WHERE id = ?");
		$stmt->execute([$targetId]);
		$todo = $stmt->fetch();

		if ($todo) {
			$newStatus = $todo['status'] ? 0 : 1;
			$stmt = $pdo->prepare("UPDATE todos SET status = ? WHERE id = ?");
			$stmt->execute([$newStatus, $targetId]);
		}
		$logger = Logger::getInstance();
		$logger->putLog("TODO_STATUS_UPDATED");
		header('Location: http://localhost/');
		exit();
	}


	/**
	 * TODOの削除(削除フラグ更新)
	 *
	 * @param [type] $post
	 * @return void
	 */
	public static function deleteTodo($post): void
	{
		$targetId = $post['id'];
		$pdo = DBconnector::getConnection();
		$stmt = $pdo->prepare("UPDATE todos SET del_flg = 1 WHERE id = ?");
		$stmt->execute([$targetId]);
		$logger = Logger::getInstance();
		$logger->putLog("TODO_DELETED");
		header('Location: http://localhost/');
		exit();
	}
}
