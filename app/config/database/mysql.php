<?php

require_once(realpath(dirname(__DIR__, 2)) . '/Core/Libs/Env.php');

use \Core\Libs\Env;

return [
	'port' 		=> 		Env::get('MYSQL_PORT'),
	'name'		=> 		Env::get('MYSQL_DATABASE'),
	'user' 		=> 		Env::get('MYSQL_USER'),
	'password' 	=> 		Env::get('MYSQL_PASSWORD'),
	'encode' 	=> 		'utf8mb4',
	'options'   => [
        \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        \PDO::ATTR_EMULATE_PREPARES   => false
    ],
];
