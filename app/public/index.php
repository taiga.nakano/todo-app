<?php
require_once(realpath(dirname(__DIR__)) . '/Core/Dispatcher.php');

use Core\Dispatcher;
use Core\Router\Router;

Dispatcher::getInstance();
$router = new Router($_SERVER['REQUEST_URI'], $_POST);
$result = $router->execute();
?>

<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="UTF-8">
	<title>TODO APP</title>
	<link rel="stylesheet" href="reset.css">
	<link rel="stylesheet" href="index.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
	<header>
		<h1>TODO</h1>
	</header>

	<main>
		<section class="add">
			<h2>Add Your Tasks</h2>
			<form action="/add" method="post">
				<div class="input-area">
					<label for="when">When</label>
					<input type="date" id="when" name="when" required>
				</div>
				<div class="input-area">
					<label for="what">What</label>
					<input type="text" id="what" name="what" maxlength="100" required>
				</div>
				<div class="add-btn">
					<button type="submit">Add</button>
				</div>
			</form>
		</section>

		<section class="list">
			<table>
				<tr>
					<th>When</th>
					<th>What</th>
					<th>Status</th>
					<th>Delete</th>
				</tr>
				<?php foreach ($result as $todo) : ?>
					<tr class="todo">
						<td class="date"><?php echo htmlspecialchars($todo['due_date'], ENT_QUOTES); ?></td>
						<td class="txt"><?php echo htmlspecialchars($todo['name'], ENT_QUOTES); ?></td>
						<td class="btn status">
							<form action="/toggle_status" method="post" style="display: inline;">
								<button type="submit" class="<?php echo $todo['status'] ? 'done' : 'active'; ?>" name="id" value="<?php echo htmlspecialchars($todo['id'], ENT_QUOTES); ?>">
									<?php echo $todo['status'] ? 'Done' : 'Active'; ?>
								</button>
							</form>

						</td>
						<td class="btn dell">
							<form action="/delete" method="post" style="display: inline;">
								<button type="submit" name="id" class="dell" value="<?php echo htmlspecialchars($todo['id'], ENT_QUOTES); ?>">
									Delete
								</button>
							</form>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
		</section>

	</main>

	<footer>
		<p>&copy; 2024 Nakano Taiga</p>
	</footer>
</body>

</html>
